using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Utils
{
    public class EfSqLiteDbInitializer : IEFDbInitializer
    {
        private readonly PromoCodeDataContext _dataContext;

        public EfSqLiteDbInitializer(PromoCodeDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Initialize()
        {
            _dataContext.Database.Migrate();
        }
    }
}