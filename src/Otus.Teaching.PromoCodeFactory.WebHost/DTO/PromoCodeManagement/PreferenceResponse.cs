using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DTO.PromoCodeManagement
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}